// Ionic Starter App
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic'])
.run(function($ionicPlatform, $ionicPopup) {
     $ionicPlatform.ready(function() {
                          if(window.cordova && window.cordova.plugins.Keyboard) {
                          // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                          // for form inputs)
                          cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                          // Don't remove this line unless you know what you are doing. It stops the viewport
                          // from snapping when text inputs are focused. Ionic handles this internally for
                          // a much nicer keyboard experience.
                          cordova.plugins.Keyboard.disableScroll(true);
                          }
                          if(window.StatusBar) {
                          StatusBar.styleDefault();
                          };
                          
                          //Function to open InAppBrowser and event for pop-up --Dimas
                          var startBrowser = function() {
                          
                          var error = 0;
                          
                          var ref = window.open('http://103.44.149.169/bntr/', '_blank', 'location=no,toolbar=no');
                          
                          ref.addEventListener('loaderror', function(event) {
                                               
                                               error = 1;
                                               ref.close();
                                               
                                               //ga masuk
                                               var alertPopup = $ionicPopup.alert({
                                                                                  title: 'Coba Lagi',
                                                                                  template: 'koneksi bermasalah!'
                                                                                  });
                                               
                                               alertPopup.then(function(res) {
                                                               //ionic.Platform.exitApp();  //doesnt run on iOS
                                                               startBrowser();
                                                               });
                                               });		
                          
                          ref.addEventListener('exit', function(event) { 
                                               
                                               if(!error){
                                               var confirmPopup = $ionicPopup.confirm({
                                                                                      title: 'Peringatan',
                                                                                      template: 'keluar aplikasi?'
                                                                                      });
                                               
                                               confirmPopup.then(function(res) {
                                                                 if(res) {
                                                                 ionic.Platform.exitApp();
                                                                 } else {
                                                                 startBrowser();
                                                                 }
                                                                 });
                                               
                                               }
                                               
                                               });		
                          };
                          
                          startBrowser();
                          
                          });
     })
